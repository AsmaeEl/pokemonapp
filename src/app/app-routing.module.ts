import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPokemonsComponent } from './pokemons/list-pokemons/list-pokemons.component';
import { DetailsPokemonComponent } from './pokemons/details-pokemon/details-pokemon.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';


const routes: Routes = [
  {path: 'pokemons', component: ListPokemonsComponent},
  {path: 'pokemons/:id', component: DetailsPokemonComponent},
  {path: '', redirectTo: 'pokemons', pathMatch:'full'},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
