import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pokemon } from '../../model/pokemon';
import { LIST_POKEMONS } from '../../shared/list.pokemons';

@Component({
  selector: 'app-details-pokemon',
  templateUrl: './details-pokemon.component.html',
  styleUrls: ['./details-pokemon.component.scss']
})
export class DetailsPokemonComponent implements OnInit {
  pokemonList: Pokemon[];
  pokemonToDisplay: Pokemon;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.pokemonList = LIST_POKEMONS;
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    for(let i = 0; i< this.pokemonList.length; i++) {
      if(this.pokemonList[i].id === id) this.pokemonToDisplay = this.pokemonList[i];
    }
  }

  editerPokemon(selectedPokemon: Pokemon) {
    console.log(selectedPokemon)
  }

  goBack(): void {
    this.router.navigate(['/pokemons'])
  }

}
