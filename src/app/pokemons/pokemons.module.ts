import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPokemonsComponent } from './list-pokemons/list-pokemons.component';
import { DetailsPokemonComponent } from './details-pokemon/details-pokemon.component';
import { PokemonTypePipe } from '../shared/pipes/pokemon-type.pipe';
import { CardHoverDirective } from '../shared/directives/card-hover.directive';



@NgModule({
  declarations: [
    PokemonTypePipe,
    CardHoverDirective,
    ListPokemonsComponent,
    DetailsPokemonComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PokemonsModule { }
