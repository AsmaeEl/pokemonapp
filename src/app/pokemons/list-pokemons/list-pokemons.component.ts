import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../../model/pokemon';
import { LIST_POKEMONS } from '../../shared/list.pokemons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-pokemons',
  templateUrl: './list-pokemons.component.html',
  styleUrls: ['./list-pokemons.component.scss']
})
export class ListPokemonsComponent implements OnInit {
  
  public pokemons: Pokemon[];

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.pokemons = LIST_POKEMONS;
  }

  selectedItem(item: Pokemon): void {
    const link = [`/pokemons/${item.id}`];
    this.router.navigate(link);
  }
}
