import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appCardHover]'
})
export class CardHoverDirective {

  @Input()userdefcolor: string ;
  MOUSE_HOVER_CLR: string = '#68ADFF';
  MOUSE_LEAVE_CLR: string = '#CEE8E9';

  constructor(private element: ElementRef) { 
    this.setBorder(this.MOUSE_LEAVE_CLR);
    this.setHeight(190);

  }

  // Definir une couleur de bordure
  private setBorder(color: string) {
    const BORDER = 'solid 3pt' + color;
    this.element.nativeElement.style.border = BORDER;
  }

  // Définir une hauteur de bordure
  private setHeight(height: number) {
    this.element.nativeElement.style.height = height + 'px';
  }

  @HostListener('mouseenter') OnMouseEnter() {
    this.setBorder(this.userdefcolor || this.MOUSE_HOVER_CLR);
   
  }

  @HostListener('mouseleave') OnMouseLeave() {
    this.setBorder(this.MOUSE_LEAVE_CLR);
  
  }
}
