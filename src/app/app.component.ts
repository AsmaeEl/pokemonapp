import { Component, OnInit } from '@angular/core';
import { Pokemon } from './model/pokemon';
import { LIST_POKEMONS } from './shared/list.pokemons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{

  public appname: string = 'UP13 pokemon app';

}
